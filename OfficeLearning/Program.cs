﻿using System;
using System.Net;
using System.Net.Mail;

namespace OfficeLearning
{
	class Program
	{
		static void Main(string[] args)
		{
			MailMessage m = new MailMessage();
			SmtpClient sc = new SmtpClient();
			try
			{
				m.From = new MailAddress("ashif.arsb@gmail.com");
				m.To.Add("ashifur.rahaman@enosisbd.com");
				m.Subject = "This is a Test Mail";
				m.IsBodyHtml = true;
				m.Body = "test gmail";
				sc.Host = "smtp.gmail.com";
				sc.Port = 587;
				sc.Credentials = new System.Net.NetworkCredential("ashif.arsb@gmail.com", "******");

				sc.EnableSsl = true;
				sc.Send(m);
				Console.WriteLine("Sent");
			}
			catch (Exception ex)
			{
				Console.WriteLine("Not Sent");
			}
			Console.ReadLine();
		}
	}
}
