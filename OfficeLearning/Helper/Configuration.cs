﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace OfficeLearning.Helper
{
	public class Configuration
	{
		private static IConfiguration config = new ConfigurationBuilder().AddJsonFile("AppSettings.json", true, true).Build();

		public static string GetConfig(string key)
		{
			return string.IsNullOrWhiteSpace(key)? "": config[key];
		}
	}
}
